# YouTube Search

This React application was created following Stephen Griders course, [Modern React With Redux](https://www.udemy.com/react-redux/), section 5.  

The learning objectives were getting acquainted with:  
- Intro to React  
- Using JSX, and ES6  
- Render method  
- Handling user events  
- Introduction to state  
- Controlled componenets  
- Class components  
- Ajax Requests  
- Props  
- Hanlding null props  
- Throttling search term  
- Styling with CSS  

#### To run the app
Checkout this repo, install dependencies, then start the gulp process with the following:

```
> git clone git clone https://yahyajideh@bitbucket.org/yahyajideh/learning.git
> cd React-Redux/Weather-App
> npm install
> npm start
```