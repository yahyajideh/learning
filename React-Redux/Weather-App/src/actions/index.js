import axios from 'axios';

const API_KEY = 'ac2ec3b9e033d78206c2a9ea57024b99';
const URL_ROOT = `http://api.openweathermap.org/data/2.5/forecast?appid=${API_KEY}`;

export const FETCH_WEATHER = 'FETCH_WEATHER';

export function fetchWeather(city) {
  const url = `${URL_ROOT}&q=${city},au`;
  const request = axios.get(url);

  return {
    type: FETCH_WEATHER, 
    payload: request
  };
}