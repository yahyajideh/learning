# Weather Forecast App

This React-Redux application was created following Stephen Griders course, [Modern React With Redux](https://www.udemy.com/react-redux/), section 5.  

The learning objectives were getting acquainted with:  
- Controlled components  
- Working with API's  
- Introduction to Middleware  
- Ajax Requests with Axios  
- Redux-Promise in practice  
- Adding Sparkline Charts  
- Google Maps integration  

#### To run the app
Checkout this repo, install dependencies, then start the gulp process with the following:

```
> git clone git clone https://yahyajideh@bitbucket.org/yahyajideh/learning.git
> cd React-Redux/Weather-App
> npm install
> npm start
```