# React Authentication App with Lock - Auth0

This application has a landing page, login page (with token retrieval), a page that only authenticated users can access, and log out. The application also uses React Router.

#### To run the app
Checkout this repo, install dependencies, then start the gulp process with the following:

```
> git clone git clone https://yahyajideh@bitbucket.org/yahyajideh/learning.git
> cd React-Redux/react-auth0
> npm install
> npm start
```