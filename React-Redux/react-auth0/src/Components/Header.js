import React from 'react';
import { Link } from 'react-router-dom';
import HeaderLogo from '../Assets/subterraAI-logo.png'
import isAuthenticated from '../Auth/isAuthenticated';

const Header = () => (
  <header>
    <div className="header-innner-left">
      <div className="logo-wrapper">
          <img src={HeaderLogo} alt="logo" />
      </div>
    </div>
    <div className="header-inner-right">
      <div className="settings-wrapper">
        {
          isAuthenticated() && ( 
            <Link to='/logout'><i className="fa fa-cog"></i></Link>
          )
        }
      </div>
    </div>
  </header>
)

export default Header;