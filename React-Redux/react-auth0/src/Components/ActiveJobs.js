import React from 'react';
import { Redirect } from 'react-router-dom';
import isAuthenticated from '../Auth/isAuthenticated';

const ActiveJobs = (props) => (
  isAuthenticated() ? (
    <div>
      <h2>Active Jobs</h2>
      <p>Hey, you’re logged in!</p>
      <iframe src="https://giphy.com/embed/3oxHQg9KunZH1iPnck" width="480" height="270" frameBorder="0" class="giphy-embed" allowFullScreen></iframe><p><a href="https://giphy.com/gifs/nba-fan-3oxHQg9KunZH1iPnck">via GIPHY</a></p>
    </div>
  ) : (
    <Redirect to={{
      pathname: '/login',
      state: { from: props.location }
    }} />
  )
)

export default ActiveJobs;