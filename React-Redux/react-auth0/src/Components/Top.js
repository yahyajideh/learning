import React, {Component} from 'react';
import Logo from '../Assets/subterraAI-logo.png';
import { Link } from 'react-router-dom';

class Top extends Component {

    render() {
        return (
            <div className="login-wrapper">
                <div className="login-logo-wrapper">
                    <img src={Logo} alt="Subterra.ai" className="login-logo"/>
                </div>
                <div className="welcom-msg">
                    <p><strong>Welcome Message</strong></p>
                </div>
                <div className="login-btn">
                    <Link to='/login'>
                      <button type="button" className="btn btn-dark">Login</button>
                    </Link>
                </div>
                <div className="forgot-pwd">
                    <a href="#">Forgot your password?</a>
                </div>
                <div className="copyright">
                    <p>&copy; Subterra.ai</p>
                </div>

            </div>
        )
    }

}

export default Top;