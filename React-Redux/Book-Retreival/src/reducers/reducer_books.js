export default function() {
    return [
        { title: 'Javascript: The Good Parts', pages: 139},
        { title: 'Harry Potter', pages: 580},
        { title: 'The Dark Tower', pages: 342},
        { title: 'Eloquent Ruby', pages: 234}, 
        { title: 'Romeo & Juliet', pages: 245 }
    ]
}