# Book Retrieval App

This React-Redux application was created following Stephen Griders course, [Modern React With Redux](https://www.udemy.com/react-redux/), section 4.  

The learning objectives were getting acquainted with:  
- Reducers  
- Containers  
- Actions and Action Creators  
- Binding Action Creators  
- Binding and Consuming Actions in Reducers  
- Conditional rendering  

#### To run the app
Checkout this repo, install dependencies, then start the gulp process with the following:

```
> git clone git clone https://yahyajideh@bitbucket.org/yahyajideh/learning.git
> cd React-Redux/Book-Retreival
> npm install
> npm start
```